# Define commands and directories
COMP_PERSO = ../compil_perso/bin/compileperso
LGIFOLDER = maths
THE_LITTLE = proof-arith.mdx

SRC_FOLDER = ./src
FIGURES_FOLDER = ../figures
MAIN_FILE = main.mdx
TEX_FOLDER = ./texfolder
AUXILIARY_FOLDER = ./aux
OUTPUT_PDF = out.pdf

# Create the output and aux directories if they don't exist
$(shell mkdir -p $(TEX_FOLDER))
$(shell mkdir -p $(AUXILIARY_FOLDER))

# Get all source files in the SRC_FOLDER
SRC_FILES = $(wildcard $(SRC_FOLDER)/*.mdx)
TEX_FILES = $(wildcard $(TEX_FOLDER)/*.tex)
BIB_FILES = $(wildcard $(SRC_FOLDER)/*.bib)
INFO_FILES = $(wildcard $(SRC_FOLDER)/*.json)

# Convert source files to .tex files in the TEX_FOLDER
SRC_TEX_FILES = $(patsubst $(SRC_FOLDER)/%.mdx, $(TEX_FOLDER)/%.tex, $(SRC_FILES))
MAIN_TEX_FILENAME = $(MAIN_FILE:.mdx=.tex)
MAIN_PDF_FILE = $(AUXILIARY_FOLDER)/$(MAIN_FILE:.mdx=.pdf)
THE_LITTLE_TEX = $(TEX_FOLDER)/$(THE_LITTLE:.mdx=_alone.tex)
THE_LITTLE_PDF = $(AUXILIARY_FOLDER)/$(THE_LITTLE:.mdx=_alone.pdf)


$(info Source .mdx files -> $(SRC_FILES))
$(info )
$(info All .tex files -> $(TEX_FILES))
$(info )
$(info .bib files -> $(BIB_FILES))
$(info )
$(info .json files -> $(INFO_FILES))
$(info )

$(info They get compiled into)
$(info Output main PDF -> $(OUTPUT_PDF))
$(info )
$(info Output little PDF -> $(THE_LITTLE_PDF))
$(info )


# Default target
all: $(OUTPUT_PDF)

the-little: $(THE_LITTLE_PDF)

# Automatic rebuild on changes using inotifywait
auto:
	fswatch -or1 $(SRC_FOLDER) $(TEX_FOLDER)| make all;
	make auto

$(THE_LITTLE_PDF): $(THE_LITTLE_TEX)
	cp $(TEX_FOLDER)/* $(AUXILIARY_FOLDER)
	cp $(SRC_FOLDER)/*.bib $(AUXILIARY_FOLDER)
	cp $(SRC_FOLDER)/*.json $(AUXILIARY_FOLDER)
	cp $(FIGURES_FOLDER)/*.pdf $(AUXILIARY_FOLDER)
	cp $(FIGURES_FOLDER)/*.png $(AUXILIARY_FOLDER)
	cd $(AUXILIARY_FOLDER); \
		latexmk -f -bibtex-cond -pdf $(patsubst($(AUXILIARY_FOLDER)/%,%,$<))

$(TEX_FOLDER)/%_alone.tex: $(SRC_FOLDER)/%.mdx
	$(COMP_PERSO) -infile $< -outfile $@ -lgifolder $(LGIFOLDER)

# Rule to generate .tex files from .mdx files
$(TEX_FOLDER)/%.tex: $(SRC_FOLDER)/%.mdx
	$(COMP_PERSO) -infile $< -outfile $@ -lgifolder $(LGIFOLDER) -bodyonly

$(TEX_FOLDER)/$(MAIN_TEX_FILENAME): $(SRC_FOLDER)/$(MAIN_FILE)
	$(COMP_PERSO) -infile $< -outfile $@ -lgifolder $(LGIFOLDER)

# Rule to generate file from .tex files
$(OUTPUT_PDF): $(SRC_TEX_FILES) $(TEX_FILES) $(BIB_FILES) $(INFO_FILES)
	cp $(TEX_FOLDER)/* $(AUXILIARY_FOLDER)
	cp $(SRC_FOLDER)/*.bib $(AUXILIARY_FOLDER)
	cp $(SRC_FOLDER)/*.json $(AUXILIARY_FOLDER)
	cp $(FIGURES_FOLDER)/*.pdf $(AUXILIARY_FOLDER)
	cp $(FIGURES_FOLDER)/*.png $(AUXILIARY_FOLDER)
	cd $(AUXILIARY_FOLDER); \
		latexmk -gg -f -bibtex-cond -pdf $(MAIN_TEX_FILENAME)
	cp $(MAIN_PDF_FILE) $(OUTPUT_PDF)

# Clean target to remove generated .tex files
clean:
	trash -f $(TEX_FOLDER)/*.tex
	trash -f $(AUXILIARY_FOLDER)/*
