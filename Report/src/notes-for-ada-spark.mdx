## What is SPARK

SPARK is an automatic prover for a subset of Ada. It compiles the Ada code to
Why3 which has tools to make automated proofs using SMT solvers\footnote{SMT
solvers used in this project are Z3, CVC5, AltErgo}. It can even sometimes find
values of the input variables for which the assertion to prove is wrong. 

\begin{center}\includegraphics[scale=0.6]{SPARKAda}\end{center}

This can be seen with the Hoare logic, a Hoare triplet is of the form P\{S\}Q
with P and Q logical formulas and S a program. There are rules to form these
triplets and if we can construct one such triplet then it means that if a
program environment $\sigma$ (this is an assignation of the program variables,
it can be seen as a memory state) satisfies P, then after executing S, the
resulting environment $\sigma '$ satisfies Q.

Here, at each Ada instruction S, with P as context, it will compute Q the
strongest postcondition ($Q = sp(P,S)$) so P\{S\}Q is a valid Hoare triple and
for any Q' such that P\{S\}Q' is a valid Hoare triple, we have $Q \implies Q'$.

When a `pragma Assert (A);` is met, for `A` a logic formula, it will try to
prove `A` with the SMT solvers from the context and then add `A` to the context
afterward.

## How to read this SPARK code

This looks like Ada code, but with added `pragma Assert (...);` to ask the SMT
solver to prove `...`,  `pragma Loop\_Invariant (...)` to ask the SMT solver to
prove a loop invariant. Associated to functions are preconditions,
postconditions, and variants (if it is recursive and we want the function to
terminate). Those are specified after the `with` keyword in the `.ads` file in
the function definition.

Lemmas are written either as boolean functions always returning `True` or as
procedures. Their assumptions are in the function's precondition and their
results are in the postcondition.

Making a call to a lemma (if the lemma is proven) will add the lemma's
postcondition to the context.

### SPARK compared to non automated proof and controling the context

Code could be compiled to properties written in Coq or any other proof assistant
so those could be proven explicitly\footnote{This is what has been used in
EasyCrypt}, but this would mean that automated proof cannot be used. However the
problem with autoactive proof is the size of the context.  When something has to
be proven by SMT solvers, if it has too much context, the search for a proof is
very big. In this internship, this proved to be a real challenge as it took a
lot of time to rewrite the proof in blocks, in lemmas, in which we select which
part of the context we need and which part we want to add to the context.

One bad way of coding in SPARK is by adding many small `pragma Assert` one after
the other, we help the prover with smaller steps to prove each time but we also
add each new assertion to the context the SMT solver we use for the next steps
so it can be slower. It is really important to control the scope of the
assertions to make sure it is not in the context at some place where it is not
necessary.

We can control this scope with lemmas, we filter the input context by writing
the necessary context as a precondition, and filter the output context by
writing the necessary postcondition for the rest of the code. Our assertions
make the body of the lemma. To control the context, we can also use
`begin`...`end` block and `pragma Assert\_And\_Cut ...some assertion...` within
the block, then, at `Assert\_And\_Cut`, all of the previous context from the
block will be forgotten to keep only `...some assertion...` to the context that
was before the block.

