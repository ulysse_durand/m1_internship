# Number Theoretic Transform

The goal is to perform fast convolution (multiplication of polynoms in $R_q$)

This is achieved by giving a ring $T_q$ and a ring isomorphism 
$\text{NTT} : R_q \-> T_q$ with $\text{NTT}$, $\text{NTT}^{-1}$ and
multiplication in $T_q$ fast to compute. Indeed, 
!equu!P * Q = \text{NTT}^{-1} (NTT(P) * NTT(Q))!equu!
We can then compute $P * Q$ in $\mathcal{O} (n \ln n)$



## Equivalence of different NTT implementations

The NTT implementation presented in ML-KEM is made to be easy to implement in
any language, without needing recursive calls or the definition of a sum.
However, it is very hard to prove properties on this code and we might want to
work on a different implementation that looks more mathematical to prove
properties on the NTT.  First, we need to specify such a reference
implementation, then to prove it is functionally equivalent to ML-KEM's NTT so
finally we can prove properties on this mathematical reference NTT so they are
true for the ML-KEM's NTT.   


Let us note $a$ the polynomial in $R_q$ we will apply NTT on, and $\hat{a} =
\text{NTT} (a)$ in $T_q$.

We will see later that $T_q$ is of the form 
$\prod_{0 \leq i < 128} \mathbb{Z}_q[X] / (P_i)$
with $P_i$ being degree two polynomials. 

Therefor we can express both $a$ and $\hat{a}$ with arrays of $\mathbb{Z}_q$ of
length $n$.

$a = \sum_{i=0}^{n-1} a_i X^i \in R_q$ and
$\hat{a} = (\hat{a}_{2j} X + \hat{a}_{2j+1})_{j=0}^{n/2 - 1}$

In the following, $\psi$ will be a $256^\text{th}$ root of the unity in
$\mathbb{Z}_q$ ($\psi^{256} = -1$). \cite{ExNTT} gives us forumlas that express
NTT and NTT$^{-1}$ (we are in the case of the Negative-Wrapped Convolution):

!equu!\hat{a}_j = \sum_{i=0}^{n-1}{\psi^{2ij+i} a_i} \qquad a_i =
 n^{-1}\sum_{j=0}^{n-1}\psi^{-(2ij+j)}\hat{a}_j!equu!

and we will use those for our reference implementation of NTT and NTT$^{-1}$.

## SPARK Reference implementation of NTT and NTT$^{-1}$ 

You can find `NTT\_Ref` in (Listing \ref{cp:nttref}). 

We can write $\text{NTT}(a, \psi) = j \mapsto \sum (i \mapsto \psi^{2ij+i} a_i)$
and this is exactly how the code is implemented. (with $\sum e$ being the sum of
all the elements of the array $e$ and $k \mapsto f(k)$ being the array with
$f(k)$ at index $k$)

!equu! \underbrace{
    j \mapsto \underbrace{
        \sum (i \mapsto \underbrace{
            \psi^{2ij+i} a_i }_{
            \text{NTT\_Very\_Inner\_Ref}
        }
        ) }_{
        \text{NTT\_Inner\_Ref} 
        }
    }_{
    \text{NTT\_Ref}
} !equu!

Efforts were made to make this an expression function (no `begin` \dots `end`
or statement, just an expression) so we can easily express it to work with it
in our proof of equivalence. 

To prove that this reference implementation is functionaly equivalent to the
ML-KEM's, we will prove it is equivalent to an intermediate implementation and
prove this implementation is equivalent to ML-KEM's implementation just as
shown in Figure \ref{fig:equiv}.

We will now see how this recursive intermediate implementation looks.

\begin{figure} \centering
\caption{Equivalences between NTT implementations}
\label{fig:equiv}
\includegraphics[scale=0.8]{equiv}
\end{figure}

## Recursive Divide and Conquer NTT

This intermediate implementation as a recursive algorithm calls NTT on the
array with even terms and the one with the odd terms of the input polynomial,
with $\psi^2$ instead of $\psi$. With this we keep the property that the second
argument to the power of the length of the first argument is equal to $-1$.

$n$ being the length of P.

\begin{algorithmic}
\Function{NTT\_Recurs}{$P, \psi$}

\State $A \leftarrow \text{NTT\_Recurs}\(P_\text{even}, \psi ^2 \)$
\State $B \leftarrow \text{NTT\_Recurs}\(P_\text{odd}, \psi ^2 \)$
\For{ $j \in \[0, \frac{n}{2}\right[$}
\State $\hat{a}_j \gets A_j + \psi^{2j+1}B_j$
\State $\hat{a}_{j+\frac{n}{2}} \gets A_j - \psi^{2j+1}B_j$
\EndFor
\EndFunction
\end{algorithmic}

This recursive algorithm computes NTT in $\mathcal{O}(n \ln n)$.

To help us understand how NTT works and how this ring decomposition leads us to
ML-KEM implementation, we will have a look at a way to see it as a ring
morphism.

## Maths behind Number Theoretic Transform


Those maths will lead us to the recursive NTT implementation that we will see
later, and it also makes the link with ML-KEM's implementation, which is non
recursive and uses the BitRev function. As there was not enough time to go
through the equivalence between the recursive NTT and ML-KEM's NTT, all that
matters in this part is the decomposition step which corresponds to the
decomposition made by the recursive function. All the explaination about binary
representation and BitRev is usefull but for the proof of equivalence we didn't
have time to go through. 

Let us decompose $(X^{256} + 1)$, with $\phi := \psi^2$ (so $\phi^{128}=-1$).

---ml
X^{256} + 1 \\
\text{(step 0)} &= (X^{256} - \phi^{128} )\\
                &= (X^{128} - \phi^{64})(X^{128} + \phi^{64}) \\
                &= (X^{128} - \phi^{64})(X^{128} - \phi^{64} * \phi^{128}) \\
\text{(step 1)} &= (X^{128} - \phi^{64}) \qquad (X^{128} - \phi^{192}) \\
\text{(step 2)} &= (X^{64} - \phi^{32}) \qquad (X^{64} - \phi^{192}) \qquad (X^{64} - \phi^{96}) \qquad (X^{64} - \phi^{224}) \\
                &\vdots
---ml

Indeed, a factor $X^r - \phi^{\text{BitRev}_8(s)}$ splits into

$(X^{r/2} - \phi^{\text{BitRev}_8(s)/2})(X^{r/2} - \phi^{\text{BitRev}_8(s)/2 + 128})$

But $\text{BitRev}_8(s)/2 = \text{BitRev}_8(2*s)$ and $\text{BitRev}_8(s)/2 + 128 = \text{BitRev}_8(2*s + 1)$

!equ! 
X^r - \phi^{\text{BitRev}_8(s)} = 
(X^{r/2} - \phi^{\text{BitRev}_8(2s)}) (X^{r/2} - \phi^{\text{BitRev}_8(2s+1)}) 
\label{eq:split}!equ!

If we write the binary representation of the power of $\phi$ in the factors at each step we have  

---ml
\text{step 0} &: 1000 0000\\
\text{step 1} &: 0100 0000, 1100 0000\\
              &\vdots\\
\text{step l} &: \text{BitRev}_8(2^l), \text{BitRev}_8(2^l+1), \dots, \text{BitRev}_8(2^{l+1} - 1)\\
              &\vdots\\
\text{step 7} &: \text{BitRev}_8(128), \dots, \text{BitRev}_8(255) \\ 
              &= 2\text{BitRev}_7(0)+1, \dots, 2\text{BitRev}_7(i)+1, \dots, 2\text{BitRev}_7(127)+1
---ml

We end up with $X^{256} + 1 = \prod_{0\leq i < 128} (X^2 - \phi^{2 \text{BitRev}_7(i) + 1})$
and this decomposition is the crucial point behind the $\text{NTT}$ ring isomorhphism.

With the Chinese Remainder Theorem, we obtain 
!equ!
R_q 
= \mathbb{Z}_q [X] / (X^{256} + 1) 
\simeq \prod_{0\leq i < 128} \mathbb{Z}_q[X]/(X^2 - \phi^{2\text{BitRev}_7(i)+1}) 
= T_q
\label{eq:ringiso}!equ!

NTT and NTT$^{-1}$ will be the isomorphism between $R_q$ and $T_q$.

In (\ref{eq:ringiso}), we have kept the order from the polynomial split
(\ref{eq:split}), because this is exactly how the recursive NTT is implemented.
In the ML-KEM's NTT, we also end up with this BitRev function.  Going into the
details of the ring isomorphism this way really makes us understand how the
recursive NTT and ML-KEM's NTT work.

### Multiplication in $T_q$


$P = (p_{0,0} X + p_{0,1} , \dots, p_{127,0} X + p_{127,1})$\\
$Q = (q_{0,0} X + q_{0,1} , \dots, q_{127,0} X + q_{127,1})$\\
---ml
(P * Q)_i &= (P_i Q_i) \text{ mod } (X^2 - \phi^{2\text{BitRev}_7(i)+1})\\
          &= (p_{i,0} q_{i,1} + p_{i,1} q_{i,0}) X + p_{i,1} q_{i,1} - p_{i,0} q_{i,0} \phi^{2\text{BitRev}_7(i) + 1}
---ml
It is computed in $\mathcal{O}(n)$
