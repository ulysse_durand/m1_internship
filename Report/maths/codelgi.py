if (not (GLOBAL_bodyonly)):
    # If -bodyonly flag is off, for the main file
    import json, os, glob

    infoexists = os.path.isfile("aux/infos.json")
    data = {}
    if infoexists:
        data = json.loads(open("aux/infos.json").read())
    def loadpackages():
        for e in glob.glob("./*.sty"):
            print("\\usepackage{"+e[2:-4]+"}")
    if infoexists:
        for key,value in data.items():
            print("\\newcommand{\\le"+key+"}{"+value+"}")
    else:
        print("\\newcommand{\\letitle}{}")
        print("\\newcommand{\\leauthor}{}")

def findlinesinfile(filepath, startpattern, endpattern):
    with open(filepath, 'r') as file:
        lines = file.readlines()
    start_line = None
    end_line = None
    for i, line in enumerate(lines):
        if start_line is None and startpattern in line:
            start_line = i + 1
        if start_line is not None and endpattern in line:
            end_line = i + 1 
            break
    return start_line, end_line