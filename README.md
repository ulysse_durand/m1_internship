# M1 Internship

This internship is about proving some properties of ML-KEM implementation in SPARK.

It was in the AdaCore company from 29/4/24 to 19/7/24, with Claire Dross and Yannick Moy.

# Report

The report can be found [here](https://gitlab.com/ulysse_durand/crypto_in_spark/-/blob/main/Report/out.pdf?ref_type=heads)

# Oral presentation

The slides for the presentation can be found [here](https://gitlab.com/ulysse_durand/crypto_in_spark/-/blob/main/presentation/out.pdf?ref_type=heads)